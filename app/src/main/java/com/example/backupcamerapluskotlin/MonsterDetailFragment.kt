package com.example.backupcamerapluskotlin

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.backupcamerapluskotlin.databinding.FragmentMonsterDetailBinding
import com.example.backupcamerapluskotlin.databinding.MonsterFragmentBinding
class MonsterDetailFragment : Fragment() {
    private lateinit var navController: NavController
    private lateinit var viewModel: MonsterViewModel
    private var _binding: FragmentMonsterDetailBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        (requireActivity() as AppCompatActivity).run{
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        setHasOptionsMenu(true)

        viewModel = ViewModelProvider(requireActivity()).get(MonsterViewModel::class.java)

        viewModel.selectedMonster.observe(viewLifecycleOwner, {
            Log.i(LOG_TAG,"Selected Monster = ${it.monsterName}")
        })

        navController = Navigation.findNavController(requireActivity(),R.id.fragmentContainerView)

        _binding = FragmentMonsterDetailBinding.inflate(inflater,container,false)
        val view = binding.root
        _binding!!.lifecycleOwner = this
        _binding!!.viewModel = viewModel
        return view
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home)
        {
            navController.navigateUp()
        }
        return super.onOptionsItemSelected(item)
    }

}