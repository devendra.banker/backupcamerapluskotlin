package com.example.backupcamerapluskotlin.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.backupcamerapluskotlin.data.MonsterRepository

class MainActivityViewModel(app:Application) : AndroidViewModel(app) {
    private val dataRepo = MonsterRepository(app)
    val monsterData = dataRepo.monsterData
}


