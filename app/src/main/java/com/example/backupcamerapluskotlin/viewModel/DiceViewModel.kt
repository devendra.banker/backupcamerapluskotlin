package com.example.backupcamerapluskotlin.viewModel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.backupcamerapluskotlin.LOG_TAG
import com.example.backupcamerapluskotlin.R
import com.example.backupcamerapluskotlin.businessLogic.Dice

//import java.io.FileReader

class DiceViewModel(app: Application) : AndroidViewModel(app){
    val headline = MutableLiveData<String>()
    val dice = MutableLiveData<IntArray>()
    private val context = app

    init {
        Log.i(LOG_TAG, "View model created")
        headline.value = context.getString(R.string.roll_em)
        dice.value = intArrayOf(6, 6, 6, 6, 6)
    }


    fun rollDice() {
        dice.value = Dice.rollDice()
        headline.value = Dice.evaluateDice(context, dice.value)
    }
}