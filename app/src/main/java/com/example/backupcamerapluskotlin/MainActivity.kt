package com.example.backupcamerapluskotlin

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import com.example.backupcamerapluskotlin.databinding.ActivityMainBinding
import com.example.backupcamerapluskotlin.databinding.ActivityMainNavBinding
import com.example.backupcamerapluskotlin.viewModel.MainActivityViewModel
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var binding: ActivityMainNavBinding
    private lateinit var mainBinding: ActivityMainBinding
    private lateinit var mainActivityViewModel: MainActivityViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainNavBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setSupportActionBar(findViewById(R.id.toolbar))
        mainActivityViewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        findViewById<Button>(R.id.rollButton).setOnClickListener {
            val intent = Intent(this, DiceActivity::class.java)
            startActivity(intent)
        }

        findViewById<Button>(R.id.selectMonsterButton).setOnClickListener {
            val intent = Intent(this, MonsterActivity::class.java)
            startActivity(intent)
        }

        binding.navView.setNavigationItemSelectedListener(this)

        addToggleButton()
        //TODO : FIX this main binding nested View
        // mainBinding = ActivityMainBinding.inflate(layoutInflater)
        //  val mainView = mainBinding.root
        // setContentView(mainView)

        mainActivityViewModel.monsterData.observe(this, {
            val monsterName = StringBuilder()
            for (monster in it) {
                monsterName.append(monster.monsterName)
                    .append("\n")
            }
            findViewById<TextView>(R.id.textView).text = monsterName
        })
    }

    private fun addToggleButton() {
        val toggle = ActionBarDrawerToggle(
            this, binding.drawerLayout, findViewById(R.id.toolbar),
            R.string.open_nav_drawer, R.string.close_nav_drawer
        )
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_about, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.action_about) {
            AboutActivity.start(this)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        binding.drawerLayout.closeDrawer(GravityCompat.START)
        when (item.itemId) {
            R.id.action_about -> {
                AboutActivity.start(this)
            }
        }
        return true
    }
}