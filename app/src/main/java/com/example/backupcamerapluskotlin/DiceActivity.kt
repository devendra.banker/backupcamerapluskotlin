package com.example.backupcamerapluskotlin

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.backupcamerapluskotlin.databinding.ActivityDiceBinding
import com.example.backupcamerapluskotlin.viewModel.DiceViewModel


class DiceActivity : AppCompatActivity() {
    private lateinit var  viewModel : DiceViewModel
    private lateinit var binding: ActivityDiceBinding
    private val imageViews by lazy {
        arrayOf<ImageView>(
            findViewById(R.id.die1),
            findViewById(R.id.die2),
            findViewById(R.id.die3),
            findViewById(R.id.die4),
            findViewById(R.id.die5)
        )
    }
    private val headline by lazy {
        findViewById<TextView>(R.id.headline)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDiceBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        viewModel = ViewModelProvider(this).get(DiceViewModel::class.java)
        setSupportActionBar(findViewById(R.id.toolbar))

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_check_circle)

        viewModel.headline.observe(this, {
            headline.text = it
        })
        viewModel.dice.observe(this, {
            updateDiceDisplay(it)
        })

        lifecycle.addObserver(MyLifecycleObserver())
        viewModel.rollDice()
    }

    private fun updateDiceDisplay(diceArray: IntArray) {
        for (i in imageViews.indices) {
            val drawableID = when (diceArray[i]) {
                1 -> R.drawable.die_1
                2 -> R.drawable.die_2
                3 -> R.drawable.die_3
                4 -> R.drawable.die_4
                5 -> R.drawable.die_5
                6 -> R.drawable.die_6
                else -> R.drawable.die_6
            }
            imageViews[i].setImageResource(drawableID)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_dice,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            R.id.action_share -> shareResult()
            else ->super.onOptionsItemSelected(item)

        }
    }

    private fun shareResult(): Boolean {
        val intent = Intent().apply{
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, "I rolled dice : ${viewModel.headline.value}")
            type = "text/plain"
        }
        startActivity(intent)
        return true
    }


}
