package com.example.backupcamerapluskotlin

const val LOG_TAG = "BCPKotlin"
const val HEADLINE_TEXT = "headline_text"
const val DICE_COLLECTION = "dice_collection"
const val WEBSERVICE_URL = "https://774906.youcanlearnit.net/"
const val IMAGE_BASE_URL = "$WEBSERVICE_URL/images/"