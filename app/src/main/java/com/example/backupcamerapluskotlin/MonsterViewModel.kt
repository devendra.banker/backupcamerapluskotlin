package com.example.backupcamerapluskotlin

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import com.example.backupcamerapluskotlin.data.Monster
import com.example.backupcamerapluskotlin.data.MonsterRepository

class MonsterViewModel (val app: Application) : AndroidViewModel(app){
    private val dataRepo = MonsterRepository(app)
    val monsterData = dataRepo.monsterData

    var selectedMonster = MutableLiveData<Monster>()
    val activityTitle = MutableLiveData<String>()

    init {
        updateActivityTitle()
    }
    fun refreshData() {
        dataRepo.refreshDataFromWeb()
    }
    fun updateActivityTitle(){
        val signature = PreferenceManager.getDefaultSharedPreferences(app).getString("signature","Super Fan")
        activityTitle.value = "Stickers for $signature"
    }

}