package com.example.backupcamerapluskotlin.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.backupcamerapluskotlin.IMAGE_BASE_URL
import com.example.backupcamerapluskotlin.WEBSERVICE_URL


@Entity(tableName = "monsters")
data class Monster (
    @PrimaryKey(autoGenerate = true)
    val monsterID : Int,
    val monsterName: String,
    val imageFile: String,
    val caption: String,
    val description: String,
    val price: Double,
    val scariness : Int
        )
{
    val imageURL
    get() = "$IMAGE_BASE_URL/$imageFile.webp"

    val thumbnailURL
        get() = "$IMAGE_BASE_URL/${imageFile}_tn.webp"
}