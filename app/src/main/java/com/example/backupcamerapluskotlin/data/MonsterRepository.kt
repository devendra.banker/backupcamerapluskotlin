package com.example.backupcamerapluskotlin.data

import android.Manifest
import android.app.Application
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.util.Log
import android.widget.Toast
import androidx.annotation.WorkerThread
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import com.example.backupcamerapluskotlin.LOG_TAG
import com.example.backupcamerapluskotlin.PERMISSION_REQUEST_CODE
import com.example.backupcamerapluskotlin.WEBSERVICE_URL

import com.example.backupcamerapluskotlin.utility.FileHelper
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class MonsterRepository(val app:Application) {
    val monsterData = MutableLiveData<List<Monster>>()
    private var listType = Types.newParameterizedType(List::class.java, Monster::class.java)

    private val monsterDao = MonsterDatabase.getDatabase(app).monsterDao()
    init {
            CoroutineScope(Dispatchers.IO).launch {
                val data = monsterDao.getAll()
                if(data.isEmpty()) {
                    getMonsterDataFromWebService()
                }else {
                    monsterData.postValue(data)
                    withContext(Dispatchers.Main){
                        Toast.makeText(app,"Using Local Sqlite Data",Toast.LENGTH_LONG).show()
                    }
                }
            }
    }
    private fun getMonsterData()
    {
        val text = FileHelper.getTextFromAssets(app,"monster_data.json")
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
        val adapter: JsonAdapter<List<Monster>> = moshi.adapter(listType)
        monsterData.value = adapter.fromJson(text) ?: emptyList()
    }

    @WorkerThread
    suspend fun getMonsterDataFromWebService()   {
        if(networkAvailable()) {
            val retrofit = Retrofit.Builder()
                .baseUrl(WEBSERVICE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
            val service = retrofit.create(MonsterService::class.java)
            val serviceData = service.getMonsterData().body() ?: emptyList()
            monsterData.postValue(serviceData)
            //saveDataToCache(serviceData)
            monsterDao.deleteAll()
            monsterDao.insertMonsters(serviceData)
            withContext(Dispatchers.Main){
                Toast.makeText(app,"Using Webservice Remote Data",Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun networkAvailable() :Boolean {
        val connectivityManager = app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo ?.isConnectedOrConnecting ?: false
    }

    fun refreshDataFromWeb() {
        CoroutineScope(Dispatchers.IO).launch {
            getMonsterDataFromWebService()
        }
    }

    fun saveDataToCache(monsterData: List<Monster>)
    {

        if(ContextCompat.checkSelfPermission(app, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            val moshi = Moshi.Builder().build()
            val listype = Types.newParameterizedType(List::class.java, Monster::class.java)
            val adapter: JsonAdapter<List<Monster>> = moshi.adapter(listType)
            val json =  adapter.toJson(monsterData)
            FileHelper.saveTextToFile(app,json)
        }

    }

    fun readDataFromCache() : List<Monster>   {
        val jsonText = FileHelper.readTextFromFile(app)
        if(jsonText == null) {
            return emptyList()
        }
        else{
            val moshi = Moshi.Builder().build()
            val listype = Types.newParameterizedType(List::class.java, Monster::class.java)
            val adapter: JsonAdapter<List<Monster>> = moshi.adapter(listType)
            return adapter.fromJson(jsonText) ?: emptyList()
        }
    }



}