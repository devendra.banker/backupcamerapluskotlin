package com.example.backupcamerapluskotlin.utility

import android.app.Application
import android.content.Context
import java.io.File
import java.nio.charset.Charset

class FileHelper {
    companion object{
        fun getTextFromResource(context: Context,resourceID:Int) : String{
            return context.resources.openRawResource(resourceID).use { it ->
                it.bufferedReader().use {
                    it.readText()
                }
            }
        }

        fun getTextFromAssets(context: Context,asset : String) : String{
            return context.assets.open(asset).use { it ->
                it.bufferedReader().use {
                    it.readText()
                }
            }
        }

        fun saveTextToFile(app: Application, json: String?) {
            val file = File(app.getExternalFilesDir("monsterData"),"monster_data.json")
            file.writeText(json?: "",Charsets.UTF_8)
        }

        fun readTextFromFile(app:Application): String? {
            val file = File(app.filesDir,"monster_data.json")
            if(file.exists())    {
                return file.readText()
            }
            else return null
        }
    }
}