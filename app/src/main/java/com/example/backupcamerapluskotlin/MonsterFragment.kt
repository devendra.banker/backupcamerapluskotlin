package com.example.backupcamerapluskotlin

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.backupcamerapluskotlin.data.Monster
import com.example.backupcamerapluskotlin.data.PrefsHelper
import com.example.backupcamerapluskotlin.databinding.ActivityMainNavBinding
import com.example.backupcamerapluskotlin.databinding.MonsterFragmentBinding

class MonsterFragment : Fragment(),MonsterRecyclerAdapter.MonsterItemListener {
    private var _binding: MonsterFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var recycleView : RecyclerView
    private lateinit var swipeLayout : SwipeRefreshLayout
    private lateinit var navController: NavController
    private lateinit var viewModel: MonsterViewModel
    private lateinit var adapter: MonsterRecyclerAdapter


    companion object {
        fun newInstance() = MonsterFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = MonsterFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        recycleView = binding.recyclerView
        val layoutStyle = PrefsHelper.getItemType(requireContext())
        recycleView.layoutManager = if (layoutStyle == "grid") {
            GridLayoutManager(requireContext(),2)
        }
        else
        {
            LinearLayoutManager(requireContext())
        }
        swipeLayout = binding.swipeLayout
        navController = Navigation.findNavController(requireActivity(),R.id.fragmentContainerView)

        setHasOptionsMenu(true)

        (requireActivity() as AppCompatActivity).run{
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }


        viewModel = ViewModelProvider(requireActivity()).get(MonsterViewModel::class.java)
        viewModel.monsterData.observe(viewLifecycleOwner, {
            adapter = MonsterRecyclerAdapter(requireContext(),it,this)
            recycleView.adapter = adapter
            swipeLayout.isRefreshing = false
        })
        viewModel.activityTitle.observe(viewLifecycleOwner,{
            requireActivity().title = it

        })

        swipeLayout.setOnRefreshListener{
            viewModel.refreshData()
        }

        return view
    }

    override fun onMonsterItemClick(monster: Monster) {
        viewModel.selectedMonster.value = monster
        navController.navigate(R.id.action_monster_to_monsterDetailFragment)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.options_main,menu)
        return super.onCreateOptionsMenu(menu,inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_view_grid -> {
                PrefsHelper.setItemType(requireContext(),"grid")
                recycleView.layoutManager =
                    GridLayoutManager(requireContext(),2)
                recycleView.adapter = adapter
            }
            R.id.action_view_list -> {
                PrefsHelper.setItemType(requireContext(),"list")
                recycleView.layoutManager =
                    LinearLayoutManager(requireContext())
                recycleView.adapter = adapter
            }
            R.id.action_settings -> {
                navController.navigate(R.id.settingsActivity)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        viewModel.updateActivityTitle()
    }
}